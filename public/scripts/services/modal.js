var mymodal = angular.module('secondarySalesApp');

mymodal.controller('ModalCtrl', function ($scope, $rootScope) {
    $scope.showModal = false;
    $scope.showModal1 = false;
    $scope.showModal2 = false;
    $scope.showModal3 = false;
    $scope.showModal4 = false;
    $scope.showModal5 = false;
    $scope.showModal6 = false;
    $scope.showModal7 = false;
    $scope.showModal8 = false;
    $scope.showModal9 = false;

    $scope.showModalExng = false;
    $scope.showModalTest = false;



    $scope.toggleModalExng = function () {
        $scope.showModalExng = !$scope.showModalExng;
    };
    $scope.toggleModalTest = function () {
        $scope.showModalTest = !$scope.showModalTest;
    };
    $scope.toggleModal = function () {
        $scope.showModal = !$scope.showModal;
    };

    $scope.toggleModalorder = function () {
        $scope.showModalorder = !$scope.showModalorder;

    };

    $scope.toggleModalcontact = function () {
        $scope.showModalcontact = !$scope.showModalcontact;
    };
    $scope.toggleModalconfirm = function () {
        $scope.showModalconfirm = !$scope.showModalconfirm;
    };

    $scope.toggleModalOrderhistory = function () {
        $scope.showModalOrderhistory = !$scope.showModalOrderhistory;
    };

    $scope.toggleModalConfirmreorder = function () {
        $scope.showModalConfirmreorder = !$scope.showModalConfirmreorder;
    };
    
       $scope.toggleModalCancledorder = function () {
        $scope.showModalCancledorder = !$scope.showModalCancledorder;
    };

     $scope.toggleModalSharingpopup = function () {
        $scope.showModalSharingpopup = !$scope.showModalSharingpopup;
    };

    $scope.toggleModallike = function () {
        $scope.showModallike = !$scope.showModallike;
    };
    $scope.toggleModal1 = function (index, item) {
        $scope.showModal1 = !$scope.showModal1;

        //////////////EarRing/////////////////
        if (item == 'item1') {
            $rootScope.display = $scope.earringone;
        }
        if (item == 'item2') {
            $rootScope.display = $scope.earringtwo;
        }
        if (item == 'item3') {
            $rootScope.display = $scope.earringthree;
        }
        if (item == 'item4') {
            $rootScope.display = $scope.earringfour;
        }
        if (item == 'item5') {
            $rootScope.display = $scope.earringfive;
        }

        //////////////Pendant/////////////////
        if (item == 'item11') {
            $rootScope.display = $scope.pendantone;
        }
        if (item == 'item12') {
            $rootScope.display = $scope.pendanttwo;
        }
        if (item == 'item13') {
            $rootScope.display = $scope.pendantthree;
        }
        if (item == 'item14') {
            $rootScope.display = $scope.pendantfour;
        }
        if (item == 'item15') {
            $rootScope.display = $scope.pendantfive;
        }

        //////////////Necklace/////////////////
        if (item == 'item21') {
            $rootScope.display = $scope.necklaceone;
        }
        if (item == 'item22') {
            $rootScope.display = $scope.necklacetwo;
        }
        if (item == 'item23') {
            $rootScope.display = $scope.necklacethree;
        }
        if (item == 'item24') {
            $rootScope.display = $scope.necklacefour;
        }
        if (item == 'item25') {
            $rootScope.display = $scope.necklacefive;
        }

        //////////////Ring/////////////////
        if (item == 'item31') {
            $rootScope.display = $scope.ringone;
        }
        if (item == 'item32') {
            $rootScope.display = $scope.ringtwo;
        }
        if (item == 'item33') {
            $rootScope.display = $scope.ringthree;
        }
        if (item == 'item34') {
            $rootScope.display = $scope.ringfour;
        }
        if (item == 'item35') {
            $rootScope.display = $scope.ringfive;
        }

        //////////////Bracelet/////////////////
        if (item == 'itemnv11') {
            $rootScope.display = $scope.braceletone;
        }
        if (item == 'itemnv12') {
            $rootScope.display = $scope.bracelettwo;
        }
        if (item == 'itemnv13') {
            $rootScope.display = $scope.braceletthree;
        }
        if (item == 'itemnv14') {
            $rootScope.display = $scope.braceletfour;
        }
        if (item == 'itemnv15') {
            $rootScope.display = $scope.braceletfive;
        }
    };


    //////////////EarRing/////////////////
    $scope.earringone = [
        {
            image: "images/image_er11.png"
        },
        {
            image: "images/image_er12.png"
        },
        {
            image: "images/image_er13.png"
        },
        {
            image: "images/image_er14.jpg"
        }
    ];

    $scope.earringtwo = [
        {
            image: "images/image_er21.png"
        },
        {
            image: "images/image_er22.png"
        },
        {
            image: "images/image_er23.png"
        },
        {
            image: "images/image_er24.jpg"
        }
    ];

    $scope.earringthree = [
        {
            image: "images/image_er31.png"
        },
        {
            image: "images/image_er32.png"
        },
        {
            image: "images/image_er33.png"
        },
        {
            image: "images/image_er34.jpg"
        }
    ];
    $scope.earringfour = [
        {
            image: "images/image_er41.png"
        },
        {
            image: "images/image_er42.png"
        },
        {
            image: "images/image_er43.png"
        },
        {
            image: "images/image_er44.jpg"
        }
    ];

    $scope.earringfive = [
        {
            image: "images/image_er51.png"
        },
        {
            image: "images/image_er52.png"
        },
        {
            image: "images/image_er53.png"
        },
        {
            image: "images/image_er54.jpg"
        }
    ];


    //////////////Pendant/////////////////
    $scope.pendantone = [
        {
            image: "images/image_pd11.png"
        },
        {
            image: "images/image_pd12.png"
        },
        {
            image: "images/image_pd13.png"
        },
        {
            image: "images/image_pd14.jpg"
        }
    ];

    $scope.pendanttwo = [
        {
            image: "images/image_pd21.png"
        },
        {
            image: "images/image_pd22.png"
        },
        {
            image: "images/image_pd23.png"
        },
        {
            image: "images/image_pd24.jpg"
        }
    ];

    $scope.pendantthree = [
        {
            image: "images/image_pd31.png"
        },
        {
            image: "images/image_pd32.png"
        },
        {
            image: "images/image_pd33.png"
        },
        {
            image: "images/image_pd34.jpg"
        }
    ];
    $scope.pendantfour = [
        {
            image: "images/image_pd41.png"
        },
        {
            image: "images/image_pd42.png"
        },
        {
            image: "images/image_pd43.png"
        },
        {
            image: "images/image_pd44.jpg"
        }
    ];

    $scope.pendantfive = [
        {
            image: "images/image_pd51.png"
        },
        {
            image: "images/image_pd52.png"
        },
        {
            image: "images/image_pd53.png"
        },
        {
            image: "images/image_pd54.jpg"
        }
    ];


    //////////////Necklace/////////////////
    $scope.necklaceone = [
        {
            image: "images/image_nk11.png"
        },
        {
            image: "images/image_nk12.png"
        },
        {
            image: "images/image_nk13.png"
        },
        {
            image: "images/image_nk14.jpg"
        }
    ];

    $scope.necklacetwo = [
        {
            image: "images/image_nk21.png"
        },
        {
            image: "images/image_nk22.png"
        },
        {
            image: "images/image_nk23.png"
        },
        {
            image: "images/image_nk24.jpg"
        }
    ];

    $scope.necklacethree = [
        {
            image: "images/image_nk31.png"
        },
        {
            image: "images/image_nk32.png"
        },
        {
            image: "images/image_nk33.png"
        },
        {
            image: "images/image_nk34.jpg"
        }
    ];
    $scope.necklacefour = [
        {
            image: "images/image_nk41.png"
        },
        {
            image: "images/image_nk42.png"
        },
        {
            image: "images/image_nk43.png"
        },
        {
            image: "images/image_nk44.jpg"
        }
    ];

    $scope.necklacefive = [
        {
            image: "images/image_nk51.png"
        },
        {
            image: "images/image_nk52.png"
        },
        {
            image: "images/image_nk53.png"
        },
        {
            image: "images/image_nk54.jpg"
        }
    ];


    //////////////Ring/////////////////
    $scope.ringone = [
        {
            image: "images/image_r11.png"
        },
        {
            image: "images/image_r12.png"
        },
        {
            image: "images/image_r13.png"
        },
        {
            image: "images/image_r14.jpg"
        }
    ];

    $scope.ringtwo = [
        {
            image: "images/image_r21.png"
        },
        {
            image: "images/image_r22.png"
        },
        {
            image: "images/image_r23.png"
        },
        {
            image: "images/image_r24.jpg"
        }
    ];

    $scope.ringthree = [
        {
            image: "images/image_r31.png"
        },
        {
            image: "images/image_r32.png"
        },
        {
            image: "images/image_r33.png"
        },
        {
            image: "images/image_r34.jpg"
        }
    ];
    $scope.ringfour = [
        {
            image: "images/image_r41.png"
        },
        {
            image: "images/image_r42.png"
        },
        {
            image: "images/image_r43.png"
        },
        {
            image: "images/image_r44.jpg"
        }
    ];

    $scope.ringfive = [
        {
            image: "images/image_r51.png"
        },
        {
            image: "images/image_r52.png"
        },
        {
            image: "images/image_r53.png"
        },
        {
            image: "images/image_r54.jpg"
        }
    ];


    //////////////Bracelet/////////////////
    $scope.braceletone = [
        {
            image: "images/image_brc11.png"
        },
        {
            image: "images/image_brc12.png"
        },
        {
            image: "images/image_brc13.png"
        },
        {
            image: "images/image_brc14.jpg"
        }
    ];

    $scope.bracelettwo = [
        {
            image: "images/image_brc21.png"
        },
        {
            image: "images/image_brc22.png"
        },
        {
            image: "images/image_brc23.png"
        },
        {
            image: "images/image_brc24.jpg"
        }
    ];

    $scope.braceletthree = [
        {
            image: "images/image_brc31.png"
        },
        {
            image: "images/image_brc32.png"
        },
        {
            image: "images/image_brc33.png"
        },
        {
            image: "images/image_brc34.jpg"
        }
    ];
    $scope.braceletfour = [
        {
            image: "images/image_brc41.png"
        },
        {
            image: "images/image_brc42.png"
        },
        {
            image: "images/image_brc43.png"
        },
        {
            image: "images/image_brc44.jpg"
        }
    ];

    $scope.braceletfive = [
        {
            image: "images/image_brc51.png"
        },
        {
            image: "images/image_brc52.png"
        },
        {
            image: "images/image_brc53.png"
        },
        {
            image: "images/image_brc54.jpg"
        }
    ];



})

mymodal.directive('cart', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +

            '<h4 class="modal-title"><span></span>&nbsp;&nbsp;&nbsp;{{ title }}</h4>' +
            '</div>' +
            '<div class="modal-body" ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});



mymodal.directive('modal1', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="orderclose()" style="float:right; "><img src="images/cross-icon-web.png" style="width:30px;height:30px;padding:4px;;border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 class="modal-title"<span></span>&nbsp;&nbsp;&nbsp;{{ title }}</h4>' +
            '</div>' +
            '<div class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});



mymodal.directive('modal2', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content" style="height: 500px;">' +
            '<div class="modal-header">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="orderclose()" style="float:right; "><img src="images/cross-icon-web.png" style="width:30px;height:30px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 style="padding:0px;font-size:25px;color:#6c6c6c;" class="modal-title"> {{ title }} </h4>' +
            '</div>' +
            '<div style="padding:0px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

mymodal.directive('modal3', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="contactclose()" style="float:right; "><img src="images/cross-icon-web.png" style="width:30px;height:30px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 style="padding:0px" class="modal-title"<span></span>&nbsp;&nbsp;&nbsp;{{ title }} </h4>' +
            '</div>' +
            '<div style="padding:0px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});


mymodal.directive('modal4', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header" style="height:60px;">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/

            '    <span aria-hidden="true" class="closebtn " ng-click="likeclose()" style="float:right; " ><img src="images/cross-icon-web.png" style="width:30px;height:30px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            //           '<h4 style="color:#878787;font-size:30px;padding:0px;">{{Orgname}}</h4>'+

            '</div>' +
            '<div style="padding-bottom:10px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
mymodal.directive('modal5', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="confirmclose()" style="float:right;"><img src="images/cross-icon-web.png" style="width:30px;height:30px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 style="padding:0px;font-size:25px;color:#6c6c6c;" class="modal-title" >{{ title }} </h4>' +
            '</div>' +
            '<div style="padding:0px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
mymodal.directive('modal6', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="historyclose()" style="float:right;"><img src="images/cross-icon-web.png" style="width:25px;height:25px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 style="padding:0px;color:#6c6c6c;" class="modal-title" >{{ title }} </h4>' +
            '</div>' +
            '<div style="padding:0px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
mymodal.directive('modal7', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header" style="background:#f4f4f4;">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="OrderConfirmclose()" style="float:right;"><img src="images/cross-icon-web.png" style="width:25px;height:25px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 style="padding:0px;color:#4d4d4d;background:#f4f4f4;" class="modal-title" >{{ title }} </h4>' +
            '</div>' +
            '<div style="padding:0px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
mymodal.directive('modal8', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header" style="background:#f4f4f4;">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/
            '    <span aria-hidden="true" class="closebtn" ng-click="OrderCanclereport()" style="float:right;"><img src="images/cross-icon-web.png" style="width:25px;height:25px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
            '<h4 style="padding:0px;color:#4d4d4d;background:#f4f4f4;" class="modal-title" >{{ title }} </h4>' +
            '</div>' +
            '<div style="padding:0px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});

mymodal.directive('modal9', function () {
    return {
        template: '<div class="modal fade">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content" style="height: 600px;">' +
            '<div class="modal-header" style="height:60px;background:#f4f4f4;">' +
            /* '<span aria-hidden="true" class="closebtn" data-dismiss="modal"><img src="images/cross-icon-web.png" ></span><span class="sr-only">Close</span>'+*/

            '    <span aria-hidden="true" class="closebtn " ng-click="Shareclose()" style="float:right; " ><img src="images/cross-icon-web.png" style="width:30px;height:30px;padding:4px; border-radius:4px;"></span><span class="sr-only">Close</span>' +
           '<h4 style="padding:0px;color:#4d4d4d;background:#f4f4f4;" class="modal-title" >{{ title }} </h4>' +

            '</div>' +
            '<div style="padding-bottom:10px" class="modal-body"  ng-transclude></div>' +
            '</div>' +
            '</div>' +
            '</div>',
        restrict: 'E',
        transclude: true,
        replace: true,
        scope: true,
        link: function postLink(scope, element, attrs) {
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function (value) {
                // console.log('value', value);
                if (value == true) {
                    //console.log('elementif', element[0]);
                    $(element).modal('show');
                    // document.getElementsByClassName("modal-dialog").modal='show';
                } else {
                    // console.log('elementelse', element[0]);
                    $(element).modal('hide');
                    //document.getElementsByClassName("modal-dialog").modal='hide';
                }
            });

            $(element).on('shown.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = true;
                });
            });

            $(element).on('hidden.bs.modal', function () {
                scope.$apply(function () {
                    scope.$parent[attrs.visible] = false;
                });
            });
        }
    };
});
