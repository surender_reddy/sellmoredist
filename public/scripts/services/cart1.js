var myApp = angular.module('secondarySalesApp');

myApp.controller('myCtrl', function ($scope, $http, ngCart, $rootScope, $modal) {
    ngCart.setTaxRate(7.5);
    ngCart.setShipping(50);
    ngCart.setPacking(20);
    $scope.simplyvegs = [
        {
            image: "images/image_1.jpg",
            name: "Margherita",
            pricerate: "Sarting at Rs.50",
            item: "item1",
            itemname: "My item 1",
            price: "10",
            description: "A hugely popular margherita, with a deliciously tangy single cheese topping."
   }
    ];

    $scope.vegtreats = [
        {
            image: "images/image_11.jpg",
            name: "Double Cheese Margherita",
            pricerate: "Sarting at Rs.100",
            item: "item11",
            itemname: "My item 11",
            price: "20",
            description: "The ever-popular Margherita - loaded with extra cheese... oodies of it!"
   },
        {
            image: "images/image_12.jpg",
            name: "Country Special",
            pricerate: "Sarting at Rs.150",
            item: "item12",
            itemname: "My item 12",
            price: "30",
            description: "For all those with a partiality for veggies, this one's loaded - crunchy onions, crisp capsicum and fresh juicy tomatoes. Yum!"
   },
        {
            image: "images/image_13.jpg",
            name: "Farm House",
            pricerate: "Sarting at Rs.150",
            item: "item13",
            itemname: "My item 13",
            price: "30",
            description: "A pizza that goes ballistic on veggies! Check out this mouth watering overload of crunchy, crisp capsicum, succulent mushrooms and fresh tomatoes"
   },
        {
            image: "images/image_14.jpg",
            name: "Spicy Triple Tango",
            pricerate: "Sarting at Rs.150",
            item: "item14",
            itemname: "My item 14",
            price: "30",
            description: "Get ready for a triple flavor treat! Sweet golden corn mingled with tangy Gherkins and luscious red paprika will make your taste buds do the tango."
   },
        {
            image: "images/image_15.jpg",
            name: "Veg Hawaii Delight",
            pricerate: "Sarting at Rs.150",
            item: "item15",
            itemname: "My item 15",
            price: "30",
            description: "Sugar and spice have never been so nice! -The sweetness of fresh pineapples, fierce punch of Jalapenos and melt-in-the mouth golden corn will transport you straight to the beaches of Hawaii."
   }

    ];

    $scope.vegspecials = [
        {
            image: "images/image_21.jpg",
            name: "Peppy Paneer",
            pricerate: "Sarting at Rs.200",
            item: "item21",
            itemname: "My item 21",
            price: "340",
            description: "Chunky paneer with crisp capsicum and spicy red pepper - quite a mouthful!"
   },
        {
            image: "images/image_22.jpg",
            name: "Maxican Green Wave",
            pricerate: "Sarting at Rs.200",
            item: "item22",
            itemname: "My item 22",
            price: "140",
            description: "A pizza loaded with crunchy onions, crisp capsicum, juicy tomatoes and jalapeno with a liberal sprinkling of exotic Mexican herbs."
   },
        {
            image: "images/image_23.jpg",
            name: "Deluxe Vaggie",
            pricerate: "Sarting at Rs.200",
            item: "item23",
            itemname: "My item 23",
            price: "240",
            description: "For a vegetarian looking for a BIG treat that goes easy on the spices, this one's got it all.. The onions, the capsicum, those delectable mushrooms - with paneer and golden corn to top it all."
   },
        {
            image: "images/image_24.jpg",
            name: "5 Pepper",
            pricerate: "Sarting at Rs.200",
            item: "item24",
            itemname: "My item 24",
            price: "540",
            description: "Dominos introduces 5 Peppers an exotic new Pizza. Topped wih red bell pepper, yellow bell pepper, capsicum, red paprika, jalapeno & sprinked with exotic herb"
   },
        {
            image: "images/image_25.jpg",
            name: "Vaggie Paradise",
            pricerate: "Sarting at Rs.200",
            item: "item25",
            itemname: "My item 25",
            price: "440",
            description: "For vegetarians who love their extravagance, we have a combination of 4 premium vegetables and cheese. Juicy Mediterranean black olives, crisp capsicum, fresh baby corn fiery Red Paprika. The Gods would probably drool over this one!"
   }

    ];

    $scope.feasts = [
        {
            image: "images/image_31.jpg",
            name: "Veg Extravaganza",
            pricerate: "Sarting at Rs.250",
            item: "item31",
            itemname: "My item 31",
            price: "10",
            description: "A pizza that decidedly staggers under an overload of golden corn, exotic black olives, crunchy onions, crisp capsicum, succulent mushrooms, juicyfresh tomatoes and jalapeno - with extra cheese to go all around."
   },
        {
            image: "images/image_32.jpg",
            name: "Cloud 9",
            pricerate: "Sarting at Rs.250",
            item: "item32",
            itemname: "My item 32",
            price: "520",
            description: "A fully loaded hurricane of tasty vegetables, this pizza is one for all seasons and reasons. Onions,juicy tomatoes, crunchy baby corn, crisp capsicum, hot jalapeno and every vegetarian’s first love: Paneer! All this on a liquid cheesy sauce base will lift your spirits higher and higher."
   },
        {
            image: "images/image_33.jpg",
            name: "Chef's Veg Wonder",
            pricerate: "Sarting at Rs.250",
            item: "item33",
            itemname: "My item 33",
            price: "510",
            description: "Not just a pizza but also a vegetarian gourmet affair! Our chef’s have put together the choicest vegetables to give you a fine dining pizza experience. Bite into a blend of tender Mushrooms, tangy Gherkins, crunchy. Babycorn, Crisp Capsicum, fiery Red Paprika, Paneer and yummy liquid cheesy sauce. Yes, all in one pizza. Wonder indeed!"
   }
    ];


    $scope.nsimplyvegs = [
        {
            image: "images/image_nv11.jpg",
            name: "Cheese & Barbeque Chicken",
            pricerate: "Sarting at Rs.50",
            item: "itemnv11",
            itemname: "My item nv12",
            price: "10",
            description: "A thrill from the grill - barbeque chicken with generous topping of cheese."
   },
        {
            image: "images/image_nv12.jpg",
            name: "Chicken Salami",
            pricerate: "Sarting at Rs.50",
            item: "itemnv12",
            itemname: "My item nv12",
            price: "10",
            description: "Who doesn’t love some cheese and Salami? An all time winning combination of exotic, seasoned chicken salami and mouthwatering cheese. What’s not to love right?"
   }
    ];

    $scope.nvegtreats = [
        {
            image: "images/image_nv21.jpg",
            name: "Barbeque Chicken",
            pricerate: "Sarting at Rs.100",
            item: "itemnv21",
            itemname: "My item nv21",
            price: "20",
            description: "A flavour of barbeque chicken spiked with onions"
   },
        {
            image: "images/image_nv22.jpg",
            name: "Spicy Chicken",
            pricerate: "Sarting at Rs.150",
            item: "itemnv22",
            itemname: "My item nv22",
            price: "30",
            description: "Hot stuff! Handle with care. This one features hot and spicy chicken with red pepper that adds an extra punch."
   },
        {
            image: "images/image_nv23.jpg",
            name: "Chicken Fiesta",
            pricerate: "Sarting at Rs.150",
            item: "itemnv23",
            itemname: "My item nv23",
            price: "30",
            description: "Dominos introduces Chicken Fiesta an exotic new Pizza. Topped with Chunky Chicken, Spicy Chicken, Capsicum & Onions."
   },
        {
            image: "images/image_nv24.jpg",
            name: "Chicken Hawaiian",
            pricerate: "Sarting at Rs.150",
            item: "itemnv24",
            itemname: "My item nv24",
            price: "30",
            description: "A perfect holiday treat! Exotic, seasoned chicken salami with delicious pineapples and juicy jalapenos will leave your taste buds happy and tingly!"
   }

    ];

    $scope.nvegspecials = [
        {
            image: "images/image_nv31.jpg",
            name: "Chicken Mexicana",
            pricerate: "Sarting at Rs.200",
            item: "itemnv31",
            itemname: "My item nv31",
            price: "340",
            description: "The taste bud-tingling of hot and spicy chicken topped with chunky onion, juicy tomato, sizzling red pepper and sprinkling of exotic Mexican herb"
   },
        {
            image: "images/image_nv32.jpg",
            name: "Chicken Golden Delight",
            pricerate: "Sarting at Rs.200",
            item: "itemnv32",
            itemname: "My item nv32",
            price: "140",
            description: "Mmm! Barbeque chicken with a topping of golden corn loaded with extra cheese. Worth its weight in gold!"
   },
        {
            image: "images/image_nv33.jpg",
            name: "Zesty Chicken",
            pricerate: "Sarting at Rs.200",
            item: "itemnv33",
            itemname: "My item nv33",
            price: "240",
            description: "Dominos introduces Zesty Chicken an exotic new Pizza. Topped with Zesty Chicken sausage, Barbeque Chicken, Capsicum & Red paprika."
   },
        {
            image: "images/image_nv34.jpg",
            name: "Chef's Chicken Choice",
            pricerate: "Sarting at Rs.200",
            item: "itemnv34",
            itemname: "My item nv34",
            price: "540",
            description: "Our chef’s have something special in store for those who love their chicken and some good veggies to go with it! Dive into a combination of Double Smoked Chicken, Tangy Black olives, Crisp Capsicum and blazing red paprika. This will get you feeling hot hot hot!"
   }

    ];

    $scope.nfeasts = [
        {
            image: "images/image_nv41.jpg",
            name: "Non Veg Supreme",
            pricerate: "Sarting at Rs.250",
            item: "itemnv41",
            itemname: "My item nv41",
            price: "10",
            description: "This is as loaded as it gets, folkes! There is hot 'n' spicy chicken with tangy black olives, onions, crisp capsicum & delectable mushrooms. Hey! Did we leave anything out?"
   },
        {
            image: "images/image_nv42.jpg",
            name: "Cheese & Pepperoni",
            pricerate: "Sarting at Rs.250",
            item: "itemnv42",
            itemname: "My item nv42",
            price: "520",
            description: "An American favourite!"
   },
        {
            image: "images/image_nv43.jpg",
            name: "Chicken Dominator",
            pricerate: "Sarting at Rs.250",
            item: "itemnv43",
            itemname: "My item nv43",
            price: "510",
            description: "This is for some serious, no-nonsense chicken lovers. If you love chicken in every form, this one’s for you. We give you smoked Chicken, Double Barbeque chicken, Exotic Chicken Salami, Hot ‘n’ Spicy Chicken & Zesty Chicken Sausage, all in one pizza with delicious liquid cheesy sauce! Let some succulent chicken dominate your taste buds!"
   },
        {
            image: "images/image_nv44.jpg",
            name: "Seventh Heaven",
            pricerate: "Sarting at Rs.250",
            item: "itemnv44",
            itemname: "My item nv44",
            price: "510",
            description: "7 ingredients: one destination- Heaven! A rich riot of color and flavor, this has got it all: Double Smoked Chicken, Double Barbeque Chicken, Zesty red and yellow bell peppers, juicy jalapenos, onions and liquid cheesy sauce. Indulge away!"
   }
    ];

    $scope.beverages = [
        {
            image: "images/image_b1.jpg",
            name: "Diet Coke",
            pricerate: "Sarting at Rs.50",
            item: "itemb1",
            itemname: "My item b11",
            price: "10",
   },
        {
            image: "images/image_b2.jpg",
            name: "Coke",
            pricerate: "Sarting at Rs.100",
            item: "itemb12",
            itemname: "My item b12",
            price: "20",
   },
        {
            image: "images/image_b3.jpg",
            name: "Fanta",
            pricerate: "Sarting at Rs.150",
            item: "itemb13",
            itemname: "My item b13",
            price: "30",
   },
        {
            image: "images/image_b4.jpg",
            name: "Sprite",
            pricerate: "Sarting at Rs.200",
            item: "itemb14",
            itemname: "My item b14",
            price: "40",
   },
        {
            image: "images/image_b5.jpg",
            name: "Zero Coke",
            pricerate: "Sarting at Rs.200",
            item: "itemb14",
            itemname: "My item b14",
            price: "40",
   }
    ];

    $scope.breads = [
        {
            image: "images/image_br1.jpg",
            name: "Garlic Breadsticks",
            pricerate: "Sarting at Rs.50",
            item: "itemnv11",
            itemname: "My item nv12",
            price: "10",
            description: "The endearing tang of garlic in breadstics baked to perfection."
   },
        {
            image: "images/image_br2.jpg",
            name: "Stuffed Garlic Bread",
            pricerate: "Sarting at Rs.50",
            item: "itemnv12",
            itemname: "My item nv12",
            price: "10",
            description: "Freshly Baked Garlic Bread stuffed with mozzarella cheese, sweet corns & tangy and spicy jalapeños"
   }
    ];

    $scope.pastas = [
        {
            image: "images/image_p1.jpg",
            name: "Veg Pasta Italiano White",
            pricerate: "Sarting at Rs.50",
            item: "itemnv11",
            itemname: "My item nv12",
            price: "10",
            description: "Penne Pasta tossed with extra virgin olive oil, exotic herbs & a generous helping of new flavoured sauce."
   },
        {
            image: "images/image_p2.jpg",
            name: "Non Veg Pasta Italiano White",
            pricerate: "Sarting at Rs.50",
            item: "itemnv12",
            itemname: "My item nv12",
            price: "10",
            description: "Penne Pasta tossed with extra virgin olive oil, exotic herbs & a generous helping of new flavoured sauce."
   }
    ];


    $scope.dips = [
        {
            image: "images/image_di1.jpg",
            name: "Cheese Jalapeno Dip",
            pricerate: "Sarting at Rs.50",
            item: "itemnv11",
            itemname: "My item nv12",
            price: "10",
            description: "A soft creamy cheese dip spiced with jalapeno."
   },
        {
            image: "images/image_d2.jpg",
            name: "Cheese Dip",
            pricerate: "Sarting at Rs.50",
            item: "itemnv12",
            itemname: "My item nv12",
            price: "10",
            description: "A dreamy creamy cheese dip to add that extra tang to your snack."
   }
    ];

    $scope.desserts = [
        {
            image: "images/image_des1.jpg",
            name: "Choco Lava Cake",
            pricerate: "Sarting at Rs.50",
            item: "itemnv11",
            itemname: "My item nv12",
            price: "10",
            description: "Filled with delecious molten chocolate inside."
   },
        {
            image: "images/image_des2.jpg",
            name: "Butterscotch Mousse Cake",
            pricerate: "Sarting at Rs.50",
            item: "itemnv12",
            itemname: "My item nv12",
            price: "10",
            description: "Soft & Creamy layered; butterscotch flavored cake"
   }
    ];

    $scope.delights = [
        {
            image: "images/image_del1.jpg",
            name: "Chicken Wings",
            pricerate: "Sarting at Rs.50",
            item: "itemnv11",
            itemname: "My item nv12",
            price: "10",
            description: "Baked chicken wings with hot and sweet sauce sure to create a toung ticking experience."
   },
        {
            image: "images/image_del2.jpg",
            name: "Spicy Baked Chicken",
            pricerate: "Sarting at Rs.50",
            item: "itemnv12",
            itemname: "My item nv12",
            price: "10",
            description: "Two juicy chicken leg pieces with spicy seasonings."
   }
    ];

    $scope.ImageClick = function (imagename) {
        console.log('Image Clicked');
        $rootScope.modalimagename = imagename;
        $rootScope.imagemodal = $modal.open({
            templateUrl: 'partials/imagemodal.html',
            windowClass: 'modal-danger',
            controller: "myCtrl"
        });

    };

    $scope.CancelImageClick = function () {
        $rootScope.imagemodal.close();
    };
});